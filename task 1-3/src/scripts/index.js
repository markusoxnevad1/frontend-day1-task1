import '../styles/index.scss';
import * as moment from 'moment';

console.log('webpack starterkit');
console.log(moment().format('LLL'));

/**
 * For more information on using Moment head to https://momentjs.com/
 */
//Task 1 - Clock
window.onload = function(){
    clock();
    setInterval(clock, 1);
};
function clock(){
    document.getElementById("clock").innerHTML = moment().format('MMMM Do YYYY, h:mm:ss a');
}

//Task 2 - Calculator
const display = document.getElementById("display");
function changeDisplay(n){
    if (display.innerHTML == 0){
        display.innerHTML = n;
    }
    else {
        display.innerHTML += n;
    }
}
function resetDisplay(n){
    display.innerHTML = n;
}
function setOperator(operator) {
    var temporary = display.innerHTML;
    display.innerHTML = temporary + operator; 
}
function calculate(){
    var res = display.innerHTML;
    display.innerHTML = eval(res);
}
global.changeDisplay = changeDisplay;
global.resetDisplay = resetDisplay;
global.setOperator = setOperator;
global.calculate = calculate;

//Task 3 - Rock, Paper, Scissor
const cpu = document.getElementById('cpu');
const res = document.getElementById('result');
function randomChoice(playerchoice) {
    var number = Math.floor(Math.random()*3);
    if (number==0) {
        cpu.innerHTML = 'CPU chooses rock';
        if(playerchoice==0){
            res.innerHTML = 'Its a tie';
        }
        if(playerchoice==1){
            res.innerHTML = 'You win';
        }
        if(playerchoice==2){
            res.innerHTML = 'You lose';
        }
    } else if (number==1) {
        cpu.innerHTML = 'CPU chooses paper';
        if(playerchoice==0){
            res.innerHTML = 'You lose';
        }
        if(playerchoice==1){
            res.innerHTML = 'Its a tie';
        }
        if(playerchoice==2){
            res.innerHTML = 'You win';
        }
    } else if (number==2) {
        cpu.innerHTML = 'CPU chooses scissor';
        if(playerchoice==0){
            res.innerHTML = 'You win';
        }
        if(playerchoice==1){
            res.innerHTML = 'You lose';
        }
        if(playerchoice==2){
            res.innerHTML = 'Its a tie';
        }
    }
}
function scoreCount(){
    const cpuScore = document.getElementById('cs');
    const playerScore = document.getElementById('ps');
    var cpuNumber = cpuScore.innerHTML;
    var playerNumber = playerScore.innerHTML;
    if (res.innerHTML == 'You win'){
        playerNumber++;
        playerScore.innerHTML = playerNumber;
    } else if (res.innerHTML == 'You lose'){
        cpuNumber++;
        cpuScore.innerHTML = cpuNumber;
    }
}
global.randomChoice = randomChoice;
global.scoreCount = scoreCount;
