// Task 1
var a = 0;
var b = 1;
for (var i=0; i<=50; i++){
    console.log(a);
    var temporary = a;
    a = b;
    b = temporary + b;
}

// Task 2
for (var i = 1; i<50; i++){
    if (i%15 == 0) {
        console.log("FizzBuzz");
    } else if (i%5 == 0) {
        console.log("Buzz");
    } else if (i%3 == 0) {
        console.log("Fizz");
    }
}